import discordapi
import os
from fastapi import FastAPI
from discord_interactions import verify_key_decorator

DISCORD_PUBLIC_KEY = os.environ.get("DISCORD_PUBLIC_KEY")


async def on_fetch(request, env):
    import asgi

    return await asgi.fetch(app, request, env)


app = FastAPI()


@app.post("/")
@verify_key_decorator(DISCORD_PUBLIC_KEY)
async def root(message):
    print(message)

