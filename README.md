# Botudo

## Getting started

* `git submodules init`
* `git submodules update`
* `wrangler dev`

## Load submodule

* Due to current limitation on Cloudflare Workers (Beta/experimental) only some python modules are supported.
* discord-interactions is not a supported module, needed to include as submodule skip it from the `requirements.txt` list.
 

